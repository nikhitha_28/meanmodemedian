package com.example.mmm;
import java.util.*;

import static java.lang.System.*;

public class meanModeMedian {
    public static void main(String[] args) {
        Scanner sc = new Scanner(in);
        int n = sc.nextInt();
        int[] arr = new int[n];
        for (int element = 0; element < n; element++) {
            arr[element] = sc.nextInt();
        }
        double mean,median=0;

        //mean calculation
        int sum=0;
        for(int element:arr){
            sum+=element;
        }
        mean=((double)sum/n);
        System.out.println(String.format("%.2f",mean));

        // median calculation
        Arrays.sort(arr);
        if (n%2==1){
        int temp=n/2;
            for(int i=0;i<n;i++){
            if (temp==i) median=arr[i];
            }
        System.out.println(String.format("%.2f",median));
        }
        else{
            median=((double)(n+1)/2);
            System.out.println(String.format("%.2f",median));
        }

        //mode calculation
        int maxCount = 1;
        for (int i = 0; i < n; i++) {
            int count = 0;
            for (int j = 0; j < n; j++) {
                if (arr[j] == arr[i])
                    count++;
            }
            if (count > maxCount) {
                maxCount = count;
                double mode = arr[i];
                System.out.println(String.format("%.2f",mode));
            }
        }

    }

}
